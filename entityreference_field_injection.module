<?php
/**
 * Vast parts of this code originate from Drupal Commerce Product Reference.
 */

/**
 * Implements hook_field_info_alter().
 */
function entityreference_field_injection_field_info_alter(&$info) {
  if (isset($info['entityreference'])) {
    $info['entityreference']['field_injection'] = FALSE;
  }
}

/**
 * Implements hook_entity_info_alter().
 *
 * Adds dedicated view modes for every field injection enabled field.
 */
function entityreference_field_injection_entity_info_alter(&$entity_info) {

  // Query the field tables directly to avoid creating a loop in the Field API:
  // it is not legal to call any of the field_info_*() in
  // hook_entity_info(), as field_read_instances() calls entity_get_info().
  $query = db_select('field_config_instance', 'fci', array('fetch' => PDO::FETCH_ASSOC));
  $query->join('field_config', 'fc', 'fc.id = fci.field_id');
  $query->fields('fci');
  $query->addField('fc', 'data', 'field_settings');
  $query->condition('fc.type', 'entityreference');
  $query->condition('fc.deleted', 0);
  $query->distinct();

  foreach ($query->execute() as $instance) {
    $instance_data = unserialize($instance['data']);
    // If the instance has the field injection enabled add view mode on
    // referencable entity type.
    if (!empty($instance_data['settings']['field_injection'])) {
      $field_settings = unserialize($instance['field_settings']);
      if (!empty($field_settings['settings']['target_type'])) {
        $entity_type = $instance['entity_type'];
        $bundle = $instance['bundle'];
        $field_name = $instance['field_name'];

        if (!empty($entity_info[$entity_type]['view modes'])) {
          foreach ($entity_info[$entity_type]['view modes'] as $view_mode => $view_mode_info) {
            $entity_info[$field_settings['settings']['target_type']]['view modes'][$entity_type . '_' . $bundle . '_' . $field_name . '_' . $view_mode] = array(
              'label' => t('@entity_type (@bundle): @field_name - @view_mode', array(
                '@entity_type' => $entity_info[$entity_type]['label'],
                '@bundle' => $entity_info[$entity_type]['bundles'][$bundle]['label'],
                '@field_name' => $instance_data['label'],
                '@view_mode' => $view_mode_info['label'],
              )),

              // UX: Enable the 'Node: teaser' mode by default, if present.
              'custom settings' => $entity_type == 'node' && $view_mode == 'teaser',
            );
          }
        }
      }
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @see entityreference_field_injection_form_field_ui_field_edit_form_validate()
 */
function entityreference_field_injection_form_field_ui_field_edit_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form['#field']['type']) && $form['#field']['type'] == 'entityreference' && isset($form['instance']['settings'])) {
    $settings = $form['#instance']['settings'];
    $form['instance']['settings']['field_injection'] = array(
      '#type' => 'checkbox',
      '#title' => t('Render fields from the referenced entity when viewing this entity.'),
      '#description' => t('If enabled, the appearance of referenced entity fields on this entity is governed by the display settings for the fields on the referenced entity type.'),
      '#default_value' => !empty($settings['field_injection']),
      '#weight' => -9,
    );
  }
}

/**
 * Finds all fields of a particular field type.
 *
 * @param string $field_type
 *   The type of field to search for.
 * @param string $entity_type
 *   Optional entity type to restrict the search to.
 *
 * @return array
 *   An array of the matching fields keyed by the field name.
 */
function entityreference_field_injection_info_fields($field_type, $entity_type = NULL) {
  $fields = array();

  // Loop through the fields looking for any fields of the specified type.
  foreach (field_info_field_map() as $field_name => $field_stub) {
    if ($field_stub['type'] == $field_type) {
      // Add this field to the return array if no entity type was specified or
      // if the specified type exists in the field's bundles array.
      if (empty($entity_type) || in_array($entity_type, array_keys($field_stub['bundles']))) {
        $field = field_info_field($field_name);
        $fields[$field_name] = $field;
      }
    }
  }

  return $fields;
}

/**
 * Returns the default referenced entity from an array of entities.
 *
 * The basic behavior for determining a default entity from an array of
 * referenced entities is to use the first referenced entity. This function
 * also allows other modules to specify a different default entity through
 * hook_entityreference_field_injection_default_delta_alter().
 *
 * @param array $entities
 *   An array of entities referenced by a entityreference field.
 * @param string $entity_type
 *   The entity type of the referencing entity.
 * @param object $referencing_entity
 *   The referencing entity.
 * @param string $field_name
 *   The field name of the processed field.
 *
 * @return object
 *   The default entity.
 */
function entityreference_field_injection_default_entity($entities, $entity_type, $referencing_entity, $field_name) {
  // Fetch the first delta value from the array.
  reset($entities);
  $delta = key($entities);

  // Allow other modules to specify a different delta value if desired.
  $context = array(
    'entity_type' => $entity_type,
    'entity' => $referencing_entity,
    'field_name' => $field_name,
  );
  drupal_alter('entityreference_field_injection_default_delta', $delta, $entities, $context);

  return $entities[$delta];
}

/**
 * Implements hook_field_extra_fields().
 *
 * This implementation will define "extra fields" on node bundles with entity
 * reference fields to correspond with available fields on entities. These
 * fields will then also be present in the node view.
 */
function entityreference_field_injection_field_extra_fields() {
  $extra = &drupal_static(__FUNCTION__);

  if (isset($extra)) {
    return $extra;
  }

  $extra = array();

  // Loop through the entity reference fields.
  foreach (entityreference_field_injection_info_fields('entityreference') as $field_name => $field) {
    foreach ($field['bundles'] as $entity_type => $bundles) {
      foreach ($bundles as $bundle_name) {
        // Get the instance settings for the field on this entity bundle.
        $instance_settings = field_info_instance($entity_type, $field['field_name'], $bundle_name);

        // If field injection is turned off for this instance, skip adding the
        // extra fields to this bundle's view modes.
        if (empty($instance_settings['settings']['field_injection'])) {
          continue;
        }

        // Get the entity type of the referenced entities.
        $referenced_entity_type = $field['settings']['target_type'];

        // Attach possible extra fields from the entity type module that may be
        // visible on the bundle. We have to call hook_field_extra_fields()
        // directly instead of using field_info_extra_fields() because of the
        // order in which these items are rebuilt in the cache for use by
        // "Manage display" tabs. Otherwise these extra fields will not appear.
        // @TODO find a sane way to figure out what actual module is behind an
        // entity type. Currently it seems impossible because there's no such
        // thing as a module entry in the entity_get_info() array.
        if (!($referenced_entity_fields = module_invoke($referenced_entity_type, 'field_extra_fields'))) {
          $referenced_entity_fields = array();
        }

        foreach ($referenced_entity_fields[$referenced_entity_type] as $key => $value) {
          if (!empty($value['display'])) {
            foreach ($value['display'] as $referenced_entity_extra_field_name => $referenced_entity_extra_field) {
              $referenced_entity_extra_field['label'] = t('@field_name: @label', array('@field_name' => $instance_settings['label'], '@label' => $referenced_entity_extra_field['label']));

              $referenced_entity_extra_field['display']['default'] = array(
                'weight' => 0,
                'visible' => FALSE,
              );

              $extra_field_key = 'referenced_entity:' . $field_name . ':' . $referenced_entity_extra_field_name;
              $extra[$entity_type][$bundle_name]['display'][$extra_field_key] = $referenced_entity_extra_field;
            }
          }
        }

        // Do the same for fields on referenced entities that may be visible on
        // the bundle.
        // First build a list of entity bundles that may be referenced.
        if (isset($field['settings']['handler_settings']['target_bundles'])) {
          $referenced_entity_bundles = array_filter($field['settings']['handler_settings']['target_bundles']);
        }
        // If no referencable entity bundles are specified, use all bundles.
        if (empty($referenced_entity_bundles)) {
          $referenced_entity_info = entity_get_info($referenced_entity_type);
          $referenced_entity_bundles = array_keys($referenced_entity_info['bundles']);
        }

        foreach ($referenced_entity_bundles as $referenced_entity_bundle) {
          foreach (field_info_instances($referenced_entity_type, $referenced_entity_bundle) as $referenced_entity_field_name => $referenced_entity_field) {
            $extra_field_key = 'referenced_entity:' . $field_name . ':' . $referenced_entity_field_name;
            $extra[$entity_type][$bundle_name]['display'][$extra_field_key] = array(
              'label' => t('@field_name: @label', array('@field_name' => $instance_settings['label'], '@label' => $referenced_entity_field['label'])),
              'description' => t('Field from a referenced entity.'),
              'weight' => $referenced_entity_field['widget']['weight'],
              'configurable' => TRUE,
              'referenced_entity_type' => $referenced_entity_type,
              'referenced_entity_bundle' => $referenced_entity_bundle,
            );
          }
        }
      }
    }
  }

  return $extra;
}

/**
 * Implements hook_entity_view().
 *
 * This implementation adds entity fields to the content array of an entity on
 * display if the entity contains a non-empty entityreference field.
 */
function entityreference_field_injection_entity_view($entity, $entity_type, $view_mode, $langcode) {
  list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);
  $instances = NULL;

  // An entity metadata wrapper will be loaded on demand if it is determined
  // this entity has a entity reference field instance attached to it.
  $wrapper = NULL;

  // Loop through entityreference fields to see if any exist on this entity
  // bundle that is either hidden.
  foreach (entityreference_field_injection_info_fields('entityreference', $entity_type) as $field_name => $field) {
    // Load the instances array only if the entity has entityreference fields.
    if (empty($instances)) {
      $instances = field_info_instances($entity_type, $bundle);
    }

    // Check if we've a valid instance with the field injection enabled.
    if (isset($instances[$field_name]) && !empty($instances[$field_name]['settings']['field_injection'])) {
      // If the field is missing a value, continue to next reference field.
      if (empty($entity->{$field_name})) {
        continue;
      }

      // Load a wrapper for the entity being viewed.
      if (empty($wrapper)) {
        $wrapper = entity_metadata_wrapper($entity_type, $entity);
      }

      // Fetch the referenced entity reference field value.
      $reference_field_value = $wrapper->{$field_name}->value();

      // If this field doesn't reference any referenced entity, continue to the
      // next one.
      if (empty($reference_field_value)) {
        continue;
      }

      // Find the default referenced entity based on the cardinality of the
      // field.
      $referenced_entity = NULL;

      if ($field['cardinality'] == 1) {
        // The value of single value reference fields is the referenced entity
        // object.
        $referenced_entity = $reference_field_value;
      }
      else {
        // Find the default from the array of all the referenced entities.
        $referenced_entity = entityreference_field_injection_default_entity($reference_field_value, $entity_type, $entity, $field_name);

        // If the entity is disabled, attempt to find one that is active and
        // use that as the default entity instead.
        if (!empty($referenced_entity) && isset($referenced_entity->status) && $referenced_entity->status == 0) {
          foreach ($reference_field_value as $reference_field_item) {
            if (!empty($reference_field_item) && isset($referenced_entity->status) && $reference_field_item->status != 0) {
              $referenced_entity = $reference_field_item;
              break;
            }
          }
        }
      }

      // If we found a referenced entity...
      if (!empty($referenced_entity)) {
        // Add the display context for these field to the referenced entity.
        $referenced_entity->display_context = array(
          'entity_type' => $entity_type,
          'entity_id' => $id,
          'entity' => $entity,
          'view_mode' => $view_mode,
          'language' => $langcode,
        );
        // Get the entity type of the refernced entities.
        $referenced_entity_type = $field['settings']['target_type'];
        list($referenced_entityid, $referenced_entityvid, $referenced_entity_bundle) = entity_extract_ids($referenced_entity_type, $referenced_entity);

        // Determine if the referenced entity type specifies custom settings
        // for the reference view mode.
        $view_mode_settings = field_view_mode_settings($referenced_entity_type, $referenced_entity_bundle);


        $reference_view_mode = $entity_type . '_' . $bundle . '_' . $field_name . '_' . $view_mode;
        if (empty($view_mode_settings[$reference_view_mode]['custom_settings'])) {
          $reference_view_mode = 'default';
        }

        // Loop through the fields on the referenced entity's type.
        foreach (field_info_instances($referenced_entity_type, $referenced_entity_bundle) as $referenced_entity_field_name => $referenced_entity_field) {
          if (!isset($referenced_entity_field['display'][$reference_view_mode])) {
            $reference_view_mode = 'default';
          }

          // Only prepare visible fields.
          if (!isset($referenced_entity_field['display'][$reference_view_mode]['type']) || $referenced_entity_field['display'][$reference_view_mode]['type'] != 'hidden') {
            // Add the referenced entity field to the entity's content array.
            $content_key = 'referenced_entity:' . $field_name . ':' . $referenced_entity_field_name;

            $entity->content[$content_key] = field_view_field($referenced_entity_type, $referenced_entity, $referenced_entity_field_name, $reference_view_mode, $langcode);

            // For multiple value references, add context information.
            if ($field['cardinality'] != 1) {
              // Construct an array of classes that will be used to theme and
              // target the rendered field for AJAX replacement.
              $classes = array(
                'entityreference-field-injection-field',
                drupal_html_class('entityreference-field-injection-field-' . $referenced_entity_field_name),
                drupal_html_class('field-' . $referenced_entity_field_name),
                drupal_html_class(implode('-', array($entity_type, $id, 'referenced-entity', $referenced_entity_field_name))),
              );

              // Add an extra class to distinguish empty referenced entity
              // fields.
              if (empty($entity->content[$content_key])) {
                $classes[] = 'entityreference-field-injection-field-empty';
              }

              // Ensure the field's content array has a prefix and suffix key.
              $entity->content[$content_key] += array(
                '#prefix' => '',
                '#suffix' => '',
              );

              // Add the custom div before and after the prefix and suffix.
              $entity->content[$content_key]['#prefix'] = '<div class="' . implode(' ', $classes) . '">' . $entity->content[$content_key]['#prefix'];
              $entity->content[$content_key]['#suffix'] .= '</div>';
            }
          }
        }

        // Get the extra fields display settings for the current view mode.
        $display = field_extra_fields_get_display($referenced_entity_type, $referenced_entity_bundle, $reference_view_mode);

        // Attach "extra fields" to the bundle representing all the extra fields
        // currently attached to entities.
        foreach (field_info_extra_fields($referenced_entity_type, $referenced_entity_bundle, 'display') as $referenced_entity_extra_field_name => $referenced_entity_extra_field) {
          // Only include extra fields that specify a theme function and that
          // are visible on the current view mode.
          if (!empty($referenced_entity_extra_field['theme']) &&
            !empty($display[$referenced_entity_extra_field_name]['visible'])) {
            // Add the entity extra field to the referencing entity's content
            // array.
            $content_key = 'referenced_entity:' . $field_name . ':' . $referenced_entity_extra_field_name;

            $entity->content[$content_key] = array(
              '#theme' => $referenced_entity_extra_field['theme'],
              '#' . $referenced_entity_extra_field_name => (isset($referenced_entity->{$referenced_entity_extra_field_name})) ? $referenced_entity->{$referenced_entity_extra_field_name} : NULL,
              '#label' => $referenced_entity_extra_field['label'] . ':',
              '#referenced_entity' => $referenced_entity,
            );

            // For multiple value references, add context information.
            if ($field['cardinality'] != 1) {
              // Construct an array of classes that will be used to theme and
              // target the rendered field for AJAX replacement.
              $classes = array(
                'entityreference-field-injection-extra-field',
                drupal_html_class('entityreference-field-injection-extra-field-' . $referenced_entity_extra_field_name),
                drupal_html_class(implode('-', array($entity_type, $id, 'referenced-entity', $referenced_entity_extra_field_name))),
              );

              // Add an extra class to distinguish empty fields.
              if (empty($entity->content[$content_key])) {
                $classes[] = 'entityreference-field-injection-extra-field-empty';
              }

              // Ensure the extra field's content array has a prefix and suffix
              // key.
              $entity->content[$content_key] += array(
                '#prefix' => '',
                '#suffix' => '',
              );

              // Add the custom div before and after the prefix and suffix.
              $entity->content[$content_key]['#prefix'] = '<div class="' . implode(' ', $classes) . '">' . $entity->content[$content_key]['#prefix'];
              $entity->content[$content_key]['#suffix'] .= '</div>';
            }
          }
        }
      }
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Override the field manage display screen to add a description to the fields
 * we embed into the target node from the product. Also implements the same hack
 * we had to use in commerce_product_reference_field_extra_fields_display_alter()
 * to default product extra fields to be hidden.
 */
function entityreference_field_injection_form_field_ui_display_overview_form_alter(&$form, &$form_state) {
  $entity_type = $form['#entity_type'];
  $bundle = $form['#bundle'];
  $view_mode = $form['#view_mode'];

  // Load the bundle settings for the current bundle.
  $bundle_settings = field_bundle_settings($entity_type, $bundle);

  // Loop over the extra fields defined by the Product Reference module for this
  // entity to set help text and make sure any extra field derived from product
  // fields to be hidden by default.
  $referenced_entity_fields = entityreference_field_injection_field_extra_fields();

  if (isset($referenced_entity_fields[$entity_type][$bundle])) {
    foreach ($referenced_entity_fields[$entity_type][$bundle]['display'] as $field_name => $field) {
      if (isset($field['referenced_entity_type'])) {
        $entity_info = entity_get_info($field['referenced_entity_type']);

        $link_text = t('@entity_type type "manage display" configuration', array('@entity_type' => $entity_info['label']));
        if (isset($entity_info['bundles'][$field['referenced_entity_bundle']]['admin']['real path'])) {
          $link_text = t('<a href="!url">@entity_type type "manage display" configuration</a>', array('@entity_type' => $entity_info['label'],'!url' => url($entity_info['bundles'][$field['referenced_entity_bundle']]['admin']['real path'])));
        }
        // If the extra field has configurable settings, add a help text for it.
        if (!empty($field['configurable'])) {
          $form['fields'][$field_name]['format']['type']['#description'] = t('Modify the settings for this field on the !link_text.', array('!link_text' => $link_text));
        }
        else {
          // Otherwise just mention it as visibility settings.
          $form['fields'][$field_name]['format']['type']['#description'] = t('The visibility of this field may also need to be toggled in the !link_text.', array('!link_text' => $link_text));

          // If no data yet exists for the extra field in the bundle settings...
          if (empty($bundle_settings['extra_fields']['display'][$field_name]) ||
            (empty($bundle_settings['view_modes'][$view_mode]['custom_settings']) && empty($bundle_settings['extra_fields']['display'][$field_name]['default'])) ||
            (!empty($bundle_settings['view_modes'][$view_mode]['custom_settings']) && empty($bundle_settings['extra_fields']['display'][$field_name][$view_mode]))) {
            // Default it to be hidden.
            $form['fields'][$field_name]['format']['type']['#default_value'] = 'hidden';
          }
        }
      }
    }
  }
}