<?php
/**
 * Vast parts of this code originate from Drupal Commerce Product Reference.
 */

/**
 * Allows modules to alter the delta value used to determine the default entity
 * in an array of referenced entities.
 *
 * The basic behavior for determining a default entity from an array of
 * referenced entities is to use the first referenced entity. This hook allows
 * modules to change that to a different delta value.
 *
 * Note that in some cases $entity will be keyed by entity ID while in other
 * cases it will be 0 indexed.
 *
 * @param string $delta
 *   The key in the $products array of the entity that should be the default
 *   entity for display purposes in a entityreference field value array.
 * @param array $entities
 *   An array of entities referenced by a entity reference field.
 * @param array $context
 *   Context information about the call. Array contains:
 *   - entity_type: The entity type of the referencing entity.
 *   - entity: The referencing entity object.
 *   - field_name: The name of the field used.
 *
 * @see entityreference_field_injection_default_entity()
 */
function hook_entityreference_field_injection_default_delta_alter(&$delta, $entities, $context) {
  // If a $entity with the SKU PROD-01 exists in the array, set that as the
  // default regardless of its position.
  foreach ($entities as $key => $entity) {
    if (isset($entity->sku) && $entity->sku == 'PROD-01') {
      $delta = $key;
    }
  }
}
